package ru.inordic.arj.gradle.dao;

import ru.inordic.arj.gradle.model.Organization;

import java.util.Set;

public interface OrganizationDao {
    void add(Organization organization) throws IllegalArgumentException;

    void delete(String snils) throws IllegalArgumentException;

    void update(Organization organization) throws IllegalArgumentException;

    Organization get(String snils) throws IllegalArgumentException;

    boolean check(String snils);

    Set<Organization> getAll();

}
