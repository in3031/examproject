package ru.inordic.arj.gradle.security;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Operator;
import ru.inordic.arj.gradle.service.SessionStorageImpl;

import java.util.Collection;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

//@Component
//@Aspect
public class RequiredRoleHandler {
    @Around("@annotation(r)")
    void handle(ProceedingJoinPoint pd, RequiredRole r) throws Throwable {
        String role = r.value();
        SecurityContext sctx = SecurityContextHolder.getContext();
        Authentication auth = sctx.getAuthentication();
        UserDetails details = (UserDetails) auth.getPrincipal();
        Set<String> roles = details.getAuthorities().stream().map(
                grantedAuthority -> grantedAuthority.getAuthority()
        ).collect(Collectors.toSet());
        if (details == null || !roles.contains(role)) {
            throw new NrdAccessDenied();
        }
        pd.proceed();
    }
}
