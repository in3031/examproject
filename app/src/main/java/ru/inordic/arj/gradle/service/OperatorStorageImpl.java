package ru.inordic.arj.gradle.service;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class OperatorStorageImpl implements OperatorStorage {
    private final List<Operator> operators = new ArrayList<>();

    public OperatorStorageImpl() {
        Operator simpleOperator = new Operator();
        simpleOperator.setLogin("vasiliy");
        simpleOperator.setPassword("123");
        simpleOperator.setRole("EMPLOYEE");
        Operator admin = new Operator();
        admin.setLogin("evgeniy");
        admin.setPassword("321");
        admin.setRole("ADMIN");
        operators.add(simpleOperator);
        operators.add(admin);
    }

    @Override
    public Operator loadByUserName(String username) {
        for (Operator o : operators) {
            if (o.getLogin().equals(username)) {
                return o;
            }
        }
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Operator op = loadByUserName(username);
        User u = new User(op.getLogin(), op.getPassword(),
                Collections.singleton(new SimpleGrantedAuthority("ROLE_"+op.getRole())));
        return u;
    }

    @Override
    public boolean verify(String username, String password) {
        for (Operator o : operators) {
            if (o.getLogin().equals(username)) {
                if (o.getPassword().equals(password)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        return false;
    }


}
