package ru.inordic.arj.gradle.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("ru.inordic.arj.gradle")
@EnableAspectJAutoProxy
public class RootConfig {
}
