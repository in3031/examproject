package ru.inordic.arj.gradle.web;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.inordic.arj.gradle.security.NrdAccessDenied;

@ControllerAdvice
public class NrdExceptionHandler {

    @ExceptionHandler(NrdAccessDenied.class)
    public String accessDenied(NrdAccessDenied e, Model model){
        model.addAttribute("reason","Недостаточно прав.");
        return "accessDenied";
    }
}
