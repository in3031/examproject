package ru.inordic.arj.gradle.util;

import org.springframework.stereotype.Component;

@Component
public class LoggerUtilImpl implements LoggerUtil {
    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }
}
