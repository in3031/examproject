package ru.inordic.arj.gradle.security;

public class NrdAccessDenied extends RuntimeException {
    public NrdAccessDenied() {
        super();
    }

    public NrdAccessDenied(String message) {
        super(message);
    }
}
