package ru.inordic.arj.gradle.config;

import com.github.mjeanroy.springmvc.view.mustache.configuration.EnableMustache;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Consumer;

@Configuration
@EnableWebMvc
@EnableMustache
public class MvcConfig implements WebMvcConfigurer {
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.stream()
                .filter(it -> it instanceof StringHttpMessageConverter)
                .forEach(it -> ((StringHttpMessageConverter) it).setDefaultCharset(StandardCharsets.UTF_8));

    }
}
