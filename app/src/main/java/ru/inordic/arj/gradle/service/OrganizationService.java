package ru.inordic.arj.gradle.service;

import ru.inordic.arj.gradle.model.Organization;

import java.util.Set;

public interface OrganizationService {
    //Set<Organization> findAllByAge(int inn);

    boolean createOrganization(String formaOrg,String nameOfOrg,
                               String inn, String kpp,
                               String ogrn, String adressUr,
                               String adressFiz,String nameOfDirector);


   // int addOneYear(String snils)  throws IllegalArgumentException;

    Set<Organization> findAll();
}
