package ru.inordic.arj.gradle.service;

import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Operator;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

//@Component("nrdSecurityFilter")
public class SessionStorageImpl extends HttpFilter implements SessionStorage {
    private final Map<UUID, Operator> sessionMap = new HashMap<>();
    private final static ThreadLocal<Operator> OPERATOR_THREAD_LOCAL = new ThreadLocal<>();

    @Override
    public void save(UUID sessionId, Operator operator) {
        sessionMap.put(sessionId, operator);
    }

    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        Cookie[] cookie = request.getCookies();
        String value = null;
        for (Cookie c : cookie) {
            if (c.getName().equals(SessionStorage.NRD_SESSION))
                value = c.getValue();

        }
        Operator operator = null;
        if (value != null) {
            UUID sessionId = UUID.fromString(value);
            operator = sessionMap.get(sessionId);
        }

        OPERATOR_THREAD_LOCAL.set(operator);
        try {
            chain.doFilter(request, response);
        } finally {
            OPERATOR_THREAD_LOCAL.remove();
        }

    }

    public static Operator getCurrentOperator() {
        return OPERATOR_THREAD_LOCAL.get();
    }
}
