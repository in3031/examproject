package ru.inordic.arj.gradle.config;


import org.apache.commons.io.IOUtils;
import org.h2.jdbcx.JdbcDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.Statement;

@Configuration
@ComponentScan("ru.inordic.arj.gradle.dao")
public class DaoConfig {
    @Bean
    DataSource dataSource() throws Exception {
//        return new EmbeddedDatabaseBuilder().
//                setType(EmbeddedDatabaseType.H2)
//                .addScript("classpath:/init.sql")
//                .build();


        JdbcDataSource ds = new JdbcDataSource();
        ds.setUrl("jdbc:h2:~/app");
        String sql = IOUtils.toString(DaoConfig.class.getResourceAsStream("/init.sql"), StandardCharsets.UTF_8);
        try (Connection cn = ds.getConnection(); Statement st = cn.createStatement()) {
            st.execute(sql);
        } catch (Exception e) {

        }
        return ds;
    }

    @Bean
    NamedParameterJdbcOperations namedParameterJdbcOperations(DataSource dataSource) {
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
