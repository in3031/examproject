package ru.inordic.arj.gradle.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import ru.inordic.arj.gradle.model.Operator;

public interface OperatorStorage extends UserDetailsService {
    boolean verify(String username, String password);

    Operator loadByUserName(String username);
}
