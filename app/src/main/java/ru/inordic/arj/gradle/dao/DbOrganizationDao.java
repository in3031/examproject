package ru.inordic.arj.gradle.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Organization;

import java.util.*;

@Component
public class DbOrganizationDao implements OrganizationDao {
    @Autowired
    NamedParameterJdbcOperations operations;
    private RowMapper<Organization> rm = new BeanPropertyRowMapper<>(Organization.class);

    @Override
    public void add(Organization organization) throws IllegalArgumentException {
        Map<String, Object> mp = new HashMap<>();
        mp.put("formaOrg", organization.getFormaOrg());
        mp.put("nameOfOrg", organization.getNameOfOrg());
        mp.put("inn", organization.getInn());
        mp.put("kpp", organization.getKpp());
        mp.put("ogrn", organization.getOgrn());
        mp.put("adressUr", organization.getAdressUr());
        mp.put("adressFiz", organization.getAdressFiz());
        mp.put("nameOfDirector", organization.getNameOfDirector());


//        operations.update("insert into app.Organization(snils,name,lastname,middlename,age)" +
//                "values(:snils,:name,:lastName,:middleName,:age)", mp);
    }

    @Override
    public void delete(String snils) throws IllegalArgumentException {
        operations.update("delete from app.human where snils=:snils",
                Collections.singletonMap("snils", snils));
    }

    @Override
    public void update(Organization organization) throws IllegalArgumentException {

    }

    @Override
    public Organization get(String snils) throws IllegalArgumentException {
        List<Organization> list = operations.query("select * from app.human", rm);
        return list.get(0);
    }

    @Override
    public boolean check(String snils) {
        return false;
    }

    @Override
    public Set<Organization> getAll() {
        return new HashSet<>(operations.query("select * from app.human", rm));
    }
}
