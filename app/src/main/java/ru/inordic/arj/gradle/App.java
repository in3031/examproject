package ru.inordic.arj.gradle;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.catalina.Context;
import org.apache.catalina.Wrapper;
import org.apache.catalina.startup.Tomcat;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;

public class App {
    public static void main(String[] args) throws Exception {
        File appBaseDir = File.createTempFile("tomcat", "appBase");
        appBaseDir.delete();
        appBaseDir.mkdir();
        File docBase = File.createTempFile("tomcat", "docBase");
        docBase.delete();
        docBase.mkdir();
        Tomcat tomcat = new Tomcat();
        tomcat.setPort(8080);
        tomcat.setBaseDir(appBaseDir.getAbsolutePath());
        tomcat.getConnector();
        Context context = tomcat.addContext("", docBase.getAbsolutePath());
        context.addServletContainerInitializer(new SpringServletContainerInitializer(),
                Set.of(MyWebInitializator.class));
        tomcat.start();
        tomcat.getServer().await();

//        ConfigurableApplicationContext ctx =
//                new AnnotationConfigApplicationContext("ru.inordic.arj.gradle");
//        WebManager bean = ctx.getBean(WebManager.class);
//        Future lock = bean.getLock();
//        lock.get();
//        ctx.close();
    }
}
