package ru.inordic.arj.gradle;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.inordic.arj.gradle.model.Organization;
import ru.inordic.arj.gradle.service.OrganizationService;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Component
public class WebManager implements InitializingBean, DisposableBean {

    @Autowired
    private OrganizationService humanService;

    private CompletableFuture<?> lock = new CompletableFuture<>();

    private HttpServer server;

    @Override
    public void afterPropertiesSet() throws Exception {

        server = HttpServer.create(new InetSocketAddress("localhost", 8081), 0);
        server.createContext("/test", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                String helloWorld = "Hello world";
                byte[] bt = helloWorld.getBytes();
                he.sendResponseHeaders(200, bt.length);
                he.getResponseBody().write(bt);
            }
        });
        server.createContext("/createRandomUser", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                humanService.createHuman("111-222-333 11", "a", "a", "a", 20);
                String response = "ok";
                byte[] bt = response.getBytes();
                he.sendResponseHeaders(200, bt.length);
                he.getResponseBody().write(bt);
            }
        });
        server.createContext("/getUser", he -> {
            Set<Organization> all = humanService.findAllByAge(20);
            String response;
            if (all.size() == 1) {
                Organization h = all.iterator().next();
                response = h.getName() + " age = " + h.getAge();
            } else {
                response = "nsdkjdvbsjdvhbvja";
            }
            byte[] bt = response.getBytes();
            he.sendResponseHeaders(200, bt.length);
            he.getResponseBody().write(bt);
        });
        server.createContext("/findByAge", he -> {
            OutputStream os = he.getResponseBody();
            StringBuilder sb = new StringBuilder();
            URI uri = he.getRequestURI();
            List<NameValuePair> params = URLEncodedUtils.parse(uri.getQuery(), null);
            int age = -1;
            boolean errorFlag = false;
            for (NameValuePair p : params) {
                if ("age".equals(p.getName())) {
                    String ageStr = p.getValue();
                    try {
                        age = Integer.parseInt(ageStr);
                    } catch (NumberFormatException e) {
                        sb.append("Wrong age format " + ageStr + "\n");
                        errorFlag = true;
                    }
                    break;
                }
            }
            int respCode;
            if (errorFlag) {
                respCode = 400;
            } else {
                Set<Organization> all = humanService.findAllByAge(age);
                sb.append("Human count with age " + age + " is " + all.size());
                for (Organization h : all) {
                    sb.append("\n" + h.getName() + " age = " + h.getAge());
                }
                respCode = 200;
            }
            byte[] bt = sb.toString().getBytes();
            he.sendResponseHeaders(respCode, bt.length);
            os.write(bt);
        });
        server.createContext("/stop", he -> {
            OutputStream os = he.getResponseBody();
            byte[] bt = "ok".getBytes();
            he.sendResponseHeaders(200, bt.length);
            os.write(bt);
            lock.complete(null);


        });

        server.createContext("/templates/index.template.html", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                OutputStream os = he.getResponseBody();
                StringBuilder sb = new StringBuilder();
                String content;
                try (InputStream rs = getClass().getResourceAsStream("/templates/index.template.html")) {
                    byte[] b = new byte[4096];
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    int size;
                    while ((size = rs.read(b)) != -1) {
                        baos.write(b, 0, size);
                    }
                    sb.append(new String(baos.toByteArray(), "UTF-8"));
                }
                Set<Organization> allPeople = humanService.findAll();
                StringBuilder tmp = new StringBuilder();
                for (Organization x : allPeople) {
                    tmp.append("<tr><td>" + x.getSnils() + "</td><td>" + x.getName() + "</td></tr> ");
                }
                content = sb.toString().replaceFirst("#allPeople#", tmp.toString());
                byte[] bt = content.getBytes();
                he.sendResponseHeaders(200, bt.length);
                os.write(bt);
            }
        });

        server.createContext("/templates/createUser.template.html", new HttpHandler() {
            @Override
            public void handle(HttpExchange he) throws IOException {
                OutputStream os = he.getResponseBody();
                StringBuilder sb = new StringBuilder();

                if (he.getRequestMethod().equals("GET")) {
                    try (InputStream rs = getClass().getResourceAsStream("/templates/createUser.template.html")) {
                        byte[] b = new byte[4096];
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int size;
                        while ((size = rs.read(b)) != -1) {
                            baos.write(b, 0, size);
                        }
                        sb.append(new String(baos.toByteArray(), "UTF-8"));
                    }
                } else {
                    {
                        InputStream body = he.getRequestBody();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int size;
                        byte[] b = new byte[4096];
                        while ((size = body.read(b)) != -1) {
                            baos.write(b, 0, size);
                        }
                        String str = new String(baos.toByteArray(), "UTF-8");
                        List<NameValuePair> params = URLEncodedUtils.parse(str, null);

                        String snils = null;
                        String name = null;
                        String lastName = null;
                        String middleName = null;
                        int age = 0;
                        for (NameValuePair p : params) {
                            switch (p.getName()) {
                                case "snils":
                                    snils = p.getValue();
                                    break;
                                case "name":
                                    name = p.getValue();
                                    break;
                                case "lastName":
                                    lastName = p.getValue();
                                    break;
                                case "middleName":
                                    middleName = p.getValue();
                                    break;
                                case "age":
                                    age = Integer.parseInt(p.getValue());
                                    break;
                            }
                        }
                        humanService.createHuman(snils, name, lastName, middleName, age);
                    }
                    try (InputStream rs = getClass().getResourceAsStream("/templates/createUserResult.template.html")) {
                        byte[] b = new byte[4096];
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        int size;
                        while ((size = rs.read(b)) != -1) {
                            baos.write(b, 0, size);
                        }
                        sb.append(new String(baos.toByteArray(), "UTF-8"));
                    }
                }


                String content = sb.toString();
                byte[] bt = content.getBytes();
                he.sendResponseHeaders(200, bt.length);
                os.write(bt);
            }
        });
        server.start();
    }


    public Future getLock() {
        return lock;
    }

    @Override
    public void destroy() throws Exception {
        server.stop(0);
    }
}
