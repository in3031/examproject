package ru.inordic.arj.gradle.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.inordic.arj.gradle.dao.OrganizationDao;
import ru.inordic.arj.gradle.model.Organization;
import ru.inordic.arj.gradle.util.LoggerUtil;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrganizationServiceImpl implements OrganizationService {

    @Autowired
   private OrganizationDao organizationDao;

  @Autowired
    private LoggerUtil loggerUtil;


    @Override
    public boolean createOrganization(String formaOrg,String nameOfOrg,
                                      String inn, String kpp,
                                      String ogrn, String adressUr,
                                      String adressFiz,String nameOfDirector) {
        if (organizationDao.check(inn)) {
            return false;
        }
        Organization h = new Organization(inn);
        h.setFormaOrg(formaOrg);
        h.setNameOfOrg(nameOfOrg);
        h.setInn(inn);
        h.setKpp(kpp);
        h.setOgrn(ogrn);
        h.setAdressUr(adressUr);
        h.setAdressFiz(adressFiz);
        h.setNameOfDirector(nameOfDirector);

        organizationDao.add(h);
        loggerUtil.printMessage("createOrganization");
        return true;
    }

//    @Override
//    public Set<Organization> findAllByAge(int age) {
//        Set<Organization> res = organizationDao.getAll().stream()
//                .filter(human -> human.getAge() == age)
//                .collect(Collectors.toSet());
//        loggerUtil.printMessage("findAllByAge");
//        return res;
//    }

    @Override
    public Set<Organization> findAll() {
        return new HashSet<>(organizationDao.getAll());
    }

//    @Override
//    public int addOneYear(String snils) throws IllegalArgumentException {
//        Organization h = organizationDao.get(snils);
//        int oldAge = h.getAge();
//        int newAge = oldAge + 1;
//        h.setAge(newAge);
//        organizationDao.update(h);
//        loggerUtil.printMessage("addOneYear");
//        return newAge;
//    }

}
