package ru.inordic.arj.gradle.model;

import java.io.Serializable;
import java.util.Objects;

public class Organization implements Serializable {
    private String formaOrg;
    private String nameOfOrg;
    private String inn;
    private String kpp;
    private String ogrn;
    private String adressUr;
    private String adressFiz;
    private String nameOfDirector;


//    private  String snils;
//    private String name;
//    private String lastName;
//    private String middleName;
//    private int age;

//    public Organization() {
//    }

    //    public Organization(int inn) {
//        this.inn = inn;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getLastName() {
//        return lastName;
//    }
//
//    public void setLastName(String lastName) {
//        this.lastName = lastName;
//    }
//
//    public String getMiddleName() {
//        return middleName;
//    }
//
//    public void setMiddleName(String middleName) {
//        this.middleName = middleName;
//    }
//
//    public int getAge() {
//        return age;
//    }
//
//    public void setAge(int age) {
//        this.age = age;
//    }
//
//    public String getSnils() {
//        return snils;
//    }
//
//    public void setSnils(String snils) {
//        this.snils = snils;
//    }
    public String getFormaOrg() {
        return formaOrg;
    }

    public void setFormaOrg(String formaOrg) {
        this.formaOrg = formaOrg;
    }

    public String getNameOfOrg() {
        return nameOfOrg;
    }

    public void setNameOfOrg(String nameOfOrg) {
        this.nameOfOrg = nameOfOrg;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getKpp() {
        return kpp;
    }

    public void setKpp(String kpp) {
        this.kpp = kpp;
    }

    public String getOgrn() {
        return ogrn;
    }

    public void setOgrn(String ogrn) {
        this.ogrn = ogrn;
    }

    public String getAdressUr() {
        return adressUr;
    }

    public void setAdressUr(String adressUr) {
        this.adressUr = adressUr;
    }

    public String getAdressFiz() {
        return adressFiz;
    }

    public void setAdressFiz(String adressFiz) {
        this.adressFiz = adressFiz;
    }

    public String getNameOfDirector() {
        return nameOfDirector;
    }

    public void setNameOfDirector(String nameOfDirector) {
        this.nameOfDirector = nameOfDirector;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Organization organization = (Organization) o;
        return inn.equals(organization.inn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inn);
    }


}
