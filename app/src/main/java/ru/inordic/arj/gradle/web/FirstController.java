package ru.inordic.arj.gradle.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.inordic.arj.gradle.model.Organization;
import ru.inordic.arj.gradle.service.OrganizationService;

import java.util.Set;

@Controller
public class FirstController {
    @Autowired
    private OrganizationService humanService;

    @GetMapping("/test")
    @ResponseBody
    public String test() {
        return "Hello world";
    }

    @GetMapping(value = "/index.html", produces = "text/html")
    public ModelAndView indexHtml(Model model) throws Exception {
        Set<Organization> allPeople = humanService.findAll();
        return new ModelAndView("index", new ExtendedModelMap().addAttribute("allPeople", allPeople));
    }


    @GetMapping(value = "/createUser.html", produces = "text/html")
//    @RequiredRole("ADMIN")
    @Secured("ROLE_ADMIN")
    public String createUser() throws Exception {
        return "createUser";
    }

    //http://localhost/orders/123
    @GetMapping(value = "/orders/{id}", produces = "text/html")
    String getBlaBla(@PathVariable("id") String id) throws Exception {
        return "createUser";
    }

    //http://localhost/createUser.html
    //Body:
    //snils=123&name=abs&middleName=asd&age=15&lastName=dbiuaiudc
    @PostMapping(value = "/createUser.html", produces = "text/html")
    public String createUserPost(@RequestParam("snils") String p1,
                                 @RequestParam("name") String name,
                                 @RequestParam("lastName") String lastName,
                                 @RequestParam("middleName") String middleName,
                                 @RequestParam("age") int age) throws Exception {
        humanService.createHuman(p1, name, lastName, middleName, age);
        return "createUserResult";
    }
}
