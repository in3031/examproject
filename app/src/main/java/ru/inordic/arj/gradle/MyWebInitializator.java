package ru.inordic.arj.gradle;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import ru.inordic.arj.gradle.config.RootConfig;

import javax.servlet.Filter;

public class MyWebInitializator extends AbstractAnnotationConfigDispatcherServletInitializer {
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[0];
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected Filter[] getServletFilters() {
        return new Filter[]{
//               new DelegatingFilterProxy("nrdSecurityFilter"),
                new DelegatingFilterProxy("springSecurityFilterChain")};
    }
}
