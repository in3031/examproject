package ru.inordic.arj.gradle;

import com.samskivert.mustache.Mustache;
import com.samskivert.mustache.Template;
import org.junit.jupiter.api.Test;

import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class MustacheTest {
    @Test
    void test() {
        Mustache.Compiler compiler = Mustache.compiler().withLoader(new Mustache.TemplateLoader() {
            @Override
            public Reader getTemplate(String name) throws Exception {
                if (name.equals("second")) {
                    StringReader sr = new StringReader("This is second template.");
                    return sr;
                }
                return null;
            }
        });
        String templateStr = "Hello, world! My name is {{name}}. " +
                "Cond is {{#cond}}true{{/cond}}{{^cond}}false{{/cond}}." +
                " {{>second}}";
        Template template = compiler.compile(templateStr);
        MyContext myContext = new MyContext();
        myContext.setName("Artyom");
        myContext.setCond(true);
        System.out.println(template.execute(myContext));
  }

    static class MyContext {
        private String name;
        private boolean cond;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isCond() {
            return cond;
        }

        public void setCond(boolean cond) {
            this.cond = cond;
        }
    }
}
