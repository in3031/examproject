package ru.inordic.arj.gradle.jpa;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class)
public class JpaTest {

    @Autowired
    private DbService dbService;
    @Autowired
    private RecordRep op;

    @Test
    void test() {
        Assertions.assertEquals(2, op.count());
        dbService.create(5, "Sidorov");
        Assertions.assertEquals(3, op.count());
        dbService.setName(5, "Subbotin");
        Assertions.assertEquals("Subbotin", op.findById(5).orElseThrow().getLastName());

    }

    @Component
    static class DbService {
        @Autowired
        private RecordRep op;

        public void create(int id, String name) {
            op.save(new Record(id, name));
        }

        @Transactional
        public void setName(int id, String newName) {
            Record rec = op.findById(id).orElseThrow();
            rec.setLastName(newName);

        }
    }
}
