package ru.inordic.arj.gradle.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RecordRep extends JpaRepository<Record, Integer> {
}
