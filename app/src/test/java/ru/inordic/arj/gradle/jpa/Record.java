package ru.inordic.arj.gradle.jpa;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "test")
public class Record {

    @Id
    private Integer id;
    @Column(name = "name")
    public String lastName;

    public Record() {
    }
    //getters+setters+constructors

    public Record(Integer id, String name) {
        this.id = id;
        this.lastName = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
