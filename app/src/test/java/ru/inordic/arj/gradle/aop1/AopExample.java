package ru.inordic.arj.gradle.aop1;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = AopExample.Config.class)
public class AopExample {

    @Autowired
    private MyService myService;

    @Test
    void test() {
        Assertions.assertThrows(Exception.class, () -> myService.callFalse());
        Assertions.assertDoesNotThrow(() -> myService.callTrue());

    }

    @Configuration
    @ComponentScan("ru.inordic.arj.gradle.aop1")
    @EnableAspectJAutoProxy
    static class Config {

    }

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    @interface MyInterceptor {
        boolean isIntercept() default false;
    }


    @Component
    @Aspect
    static class NrdAspect {
        private static final ThreadLocal<MyInterceptor> val = new ThreadLocal<>();

        @Around("@annotation(s)")
        public void handle(ProceedingJoinPoint pd, MyInterceptor s) throws Throwable {
            System.out.println("start call");
            System.out.println("isIntercept = " + s.isIntercept());
            val.set(s);
            pd.proceed();
            System.out.println("end call");
        }

        public static MyInterceptor getForCurrentThread() {
            return val.get();
        }

    }

    @Component
    static class Called {
        public void call() {
            MyInterceptor s = NrdAspect.getForCurrentThread();
            if (!s.isIntercept())
                throw new IllegalStateException();
        }
    }

    @Component
    static class MyService {

        @Autowired
        private Called called;

        @MyInterceptor(isIntercept = false)
        public void callFalse() {
            System.out.println("-----call");
            called.call();
        }

        @MyInterceptor(isIntercept = true)
        public void callTrue() {
            System.out.println("-----call");
            called.call();
        }
    }


}
