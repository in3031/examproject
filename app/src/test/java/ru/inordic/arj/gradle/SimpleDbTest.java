package ru.inordic.arj.gradle;

import org.h2.jdbcx.JdbcDataSource;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.*;
import java.util.Collections;

public class SimpleDbTest {
    @Test
    void test() throws Exception {
        JdbcDataSource ds = new JdbcDataSource();
        ds.setUrl("jdbc:h2:mem:test1");
        try (Connection con = ds.getConnection()) {
            try (Statement st = con.createStatement()) {
                st.execute("Create schema PRODUCT");
                st.execute("Create sequence test_seq");
                st.execute("Create table PRODUCT.ANIMALS (id int primary key,age int, name varchar) ");
                st.execute("insert into product.animals (id,age,name) values (1, 10, 'caw')");
                try (ResultSet rs = st.executeQuery("SELECT age, name FROM PRODUCT.ANIMALS")) {
                    while (rs.next()) {
                        String name = rs.getString("name");
                        System.out.println("name = " + name);
                    }
                }
            }
            try (PreparedStatement preparedStatement = con
                    .prepareStatement("insert into product.animals (id,age,name) values (?, ?, ?)")) {
                preparedStatement.setInt(1, 10);
                preparedStatement.setInt(2, 5);
                preparedStatement.setString(3, "dog");
                preparedStatement.executeUpdate();

            }
            NamedParameterJdbcTemplate operations = new NamedParameterJdbcTemplate(ds);

            Integer res = operations.queryForObject("select test_seq.nextval", Collections.emptyMap(), Integer.class);
            Integer res2 = operations.queryForObject("select test_seq.nextval", Collections.emptyMap(), Integer.class);

            System.out.println("=========== res=" + res);
            System.out.println("=========== res=" + res2);
        }
    }
}
