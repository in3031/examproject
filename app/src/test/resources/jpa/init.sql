create table test
(
    id   int primary key,
    name varchar
);
insert into test (id, name)
values (1, 'Ivanov');
insert into test (id, name)
values (2, 'Petrov');